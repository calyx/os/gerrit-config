#!/usr/bin/env python3

import yaml
import requests

from github import Github
from gitlab import Gitlab

from lib import Gerrit, Config

if Config.GERRIT_USER and Config.GERRIT_PASS:
    auth = requests.auth.HTTPBasicAuth(Config.GERRIT_USER, Config.GERRIT_PASS)
else:
    auth = None

print("Updating gerrit permissions...")

with open("structure.yml", "r") as f:
    desired_projects = yaml.load(f.read(), Loader=yaml.BaseLoader)

live_projects = Gerrit.get_projects(auth)
permission_projects = Gerrit.get_perm_projects(auth)

changes = {}

for parent, children in desired_projects.items():
    if parent in live_projects:
        if set(live_projects[parent]) == set(desired_projects[parent]):
            continue
        else:
            changes[parent] = list(set(desired_projects[parent]) - set(live_projects[parent]))
            if not changes[parent]:
                del changes[parent]
    else:
        changes[parent] = children

if changes:
    for parent, children in changes.items():
        for child in children:
            if not any(any(child in p for p in projects) for projects in live_projects.values()):
                Gerrit.create_project(child, auth)
            Gerrit.update_parent(child, parent, auth)

print("Creating github and gitlab repos...")

gh = Github(Config.GITHUB_TOKEN)
gl = Gitlab(f'https://gitlab.com', Config.GITLAB_TOKEN)
gl_group = gl.groups.get(Config.GITLAB_GROUP_ID)

gh_team = gh.get_organization("CalyxOS").get_team_by_slug("calyxos-gerrit")
gh_repos = gh.get_organization("CalyxOS").get_repos()

github_projects = {x.full_name for x in gh_repos}
gitlab_projects = {x.path_with_namespace for x in gl_group.projects.list(all=True, archived=False, visibility="public", include_subgroups=False)}
gitlab_private_projects = {x.path_with_namespace for x in gl_group.projects.list(all=True, archived=False, visibility="private", include_subgroups=False)}
gerrit_projects = set()
private_projects = set()
for parent, children in live_projects.items():
    if parent.startswith("CalyxOS/"):
        gerrit_projects.add(parent)
    for child in children:
        if parent == "Private-Projects":
            private_projects.add(child)
        if child.startswith("CalyxOS/"):
            gerrit_projects.add(child)

for parent, children in permission_projects.items():
    private_projects.add("CalyxOS/" + parent)

github_public_missing = gerrit_projects - github_projects - private_projects
gitlab_public_missing = gerrit_projects - gitlab_projects - private_projects
github_private_missing = gerrit_projects - github_projects - github_public_missing

for repo in github_public_missing:
    try:
        print(f"Creating {repo} on github...")
        gh.get_organization("CalyxOS").create_repo(repo.replace("CalyxOS/",""), has_wiki=False, has_downloads=False, has_projects=False, has_issues=False, private=False)
    except Exception as e:
        print(f"Failed creating {repo} on GitHub")
        pass

for repo in github_private_missing:
    try:
        print(f"Creating {repo} on github...")
        gh.get_organization("CalyxOS").create_repo(repo.replace("CalyxOS/",""), has_wiki=False, has_downloads=False, has_projects=False, has_issues=False, private=True)
    except Exception as e:
        print(f"Failed creating {repo} on GitLab")
        pass

for repo in gitlab_public_missing:
    try:
        if repo in gitlab_private_projects:
            print(f"Setting {repo} to public on gitlab...")
            gl_project = gl.projects.get(repo)
            gl_project.visibility = "public"
            gl_project.merge_requests_enabled = False
            gl_project.issues_enabled = False
            gl_project.save()
        else:
            print(f"Creating {repo} on gitlab...")
            gl.projects.create({'name': repo.replace("CalyxOS/",""), 'namespace_id': Config.GITLAB_GROUP_ID})
    except Exception as e:
        print(f"Failed setting {repo} to public on GitLab")
        pass

for repo in github_projects:
    try:
        perm = gh_team.get_repo_permission(repo)
        if perm is None or not perm.push:
            print(f"Adding {repo} to calyxos-gerrit GitHub team")
            [_repo] = [x for x in gh_repos if x.full_name == repo]
            gh_team.add_to_repos(_repo)
            gh_team.update_team_repository(_repo, "push")
    except Exception as e:
        print(f"Failed adding {repo}  to calyxos-gerrit GitHub team")
        pass

print("Done!")
