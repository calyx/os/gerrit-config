#!/usr/bin/env python3

import requests

from lib import Gerrit, Config

if Config.GERRIT_USER and Config.GERRIT_PASS:
    auth = requests.auth.HTTPBasicAuth(Config.GERRIT_USER, Config.GERRIT_PASS)
else:
    auth = None

projects = Gerrit.get_perm_projects(auth)

for node in sorted(projects.keys()):
    print(f"{node}")

