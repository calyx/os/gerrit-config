#!/usr/bin/env python3

import yaml
import requests

from lib import Gerrit, Config

if Config.GERRIT_USER and Config.GERRIT_PASS:
    auth = requests.auth.HTTPBasicAuth(Config.GERRIT_USER, Config.GERRIT_PASS)
else:
    auth = None

android12_projects = set(Gerrit.get_project_having_branch("android12", auth))
android11_projects = set(Gerrit.get_project_having_branch("android11-qpr1", auth)) - android12_projects
android10_projects = set(Gerrit.get_project_having_branch("android10", auth)) - android11_projects
main_projects = set(Gerrit.get_project_having_branch("master", auth)) - android10_projects

for project in main_projects:
	print(project)
