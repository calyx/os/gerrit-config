#!/usr/bin/env python3

import yaml
import requests

import github
import gitlab

from lib import Gerrit, Config

if Config.GERRIT_USER and Config.GERRIT_PASS:
    auth = requests.auth.HTTPBasicAuth(Config.GERRIT_USER, Config.GERRIT_PASS)
else:
    auth = None

print("Setting repository defaults and protecting branches")

android13_projects = set(Gerrit.get_project_having_branch("android13", auth))
android12L_projects = set(Gerrit.get_project_having_branch("android12L", auth)) - android13_projects
android11_projects = set(Gerrit.get_project_having_branch("android11-qpr1", auth)) - android12L_projects
android10_projects = set(Gerrit.get_project_having_branch("android10", auth)) - android11_projects
main_projects = set(Gerrit.get_project_having_branch("main", auth)) - android10_projects

gh = github.Github(Config.GITHUB_TOKEN)
gl = gitlab.Gitlab(f'https://gitlab.com', Config.GITLAB_TOKEN)
gl_group = gl.groups.get(Config.GITLAB_GROUP_ID)
gitlab_projects = gl_group.projects.list(all=True, archived=False, include_subgroups=True)

gh_repos = gh.get_organization("CalyxOS").get_repos()


def handle_gitlab_features():
    # Defaults for all GitLab projects
    for project in gitlab_projects:
        try:
            gl_project = gl.projects.get(project.id)
            print("Handling: " + gl_project.path)
            gl_project.merge_requests_enabled = False
            if gl_project.id != Config.GITLAB_CALYXOS_PROJECT_ID:
                gl_project.issues_enabled = False
            gl_project.save()
        except Exception as e:
            print("Failed handling: " + project)
            print(e)


def handle_default_branch(project, branch):
    try:
        print(f"Setting {branch} to default for {project}")
        gl_project = gl.projects.get(project)
        gl_project.default_branch = branch
        gl_project.save()

        # This project is too big for GitHub
        if project == "CalyxOS/platform_prebuilts_calyx":
            return

        [gh_project] = [x for x in gh_repos if x.full_name == project]
        gh_project.edit(default_branch=branch)
    except Exception as e:
        print(f"Failed setting {branch} to default for {project}")
        print(e)

def handle_protected_branch(project, branch):
    try:
        print(f"Protecting {branch} for {project}")
        gl_project = gl.projects.get(project)
        p_branch = gl_project.protectedbranches.get(branch)
        if not p_branch:
            p_branch = gl_project.protectedbranches.create({
                'name': f'{branch}',
                'merge_access_level': gitlab.const.MAINTAINER_ACCESS,
                'push_access_level': gitlab.const.MAINTAINER_ACCESS
            })

        # This project is too big for GitHub
        if project == "CalyxOS/platform_prebuilts_calyx":
            return

        [gh_project] = [x for x in gh_repos if x.full_name == project]

        # Upgrade to GitHub Pro or make this repository public to enable this feature
        if project.startswith("CalyxOS/proprietary_"):
            return
        gh_branch = gh_project.get_branch(branch)
        gh_branch.edit_protection(strict=True)
    except Exception as e:
        print(f"Failed protecting {branch} for {project}")
        print(e)

def handle_project(project, branch):
    handle_default_branch(project, branch)
    handle_protected_branch(project, branch)

# Disable Merge Requests and Issues
handle_gitlab_features()

# Set default branch and protect it
for project in main_projects:
    handle_project(project, "main")

for project in android13_projects:
    handle_project(project, "android13")
